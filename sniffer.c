#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <strings.h>
#include <string.h>
#include <wsuart.h>
#include <dis.h>
#include <mac_defs.h>
#include <sniffer.h>


#ifdef MAC_LPWMN_ID_ELIDED
#undef MAC_LPWMN_ID_LEN
#define MAC_LPWMN_ID_LEN  0
#endif

int verbose = 0;

int channNr;       

int channCfgDone = 0;

#define SER_BUFF_LEN  256

#define LPWMN_EXT_ADDR_LEN  8

unsigned char serTxBuff[SER_BUFF_LEN];
unsigned char serRxBuff[SER_BUFF_LEN];

static cntxt_s uart_cntxt;

int listTblIdx = 0;
                  
int hdrAcked = 0, allEntriesListed = 0;

#define POLY 0x1021  // 0x8408
/*
//                                      16   12   5
// this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
// This works out to be 0x1021, but the way the algorithm works
// lets us use 0x8408 (the reverse of the bit pattern).  The high
// bit is always assumed to be set, thus we only use 16 bits to
// represent the 17 bit value.
*/


time_t frameRxTime;
   
unsigned short expDisMsgSrcShortAddr = LPWMN_BROADCAST_SHORT_ADDR;

unsigned short crc16(unsigned char *buff_p, unsigned char len)
{
   unsigned int ckSum = 0;

   while (len > 1)
   {
      unsigned short tmp = *buff_p;
      tmp = (tmp << 8) | (*(buff_p + 1));
      ckSum = ckSum + tmp;
      buff_p += 2;
      len -= 2;
   }

   if (len > 0)
       ckSum += (*buff_p);

   while (ckSum >> 16)
   {
      ckSum = (ckSum & 0xffff) + (ckSum >> 16);
   }

   return (~ckSum);
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
unsigned char TLV_get(unsigned char *buff_p, unsigned char len, unsigned char type,
                      unsigned char *pyldLen_p, unsigned char **pyldBuff_pp)
{
    int buffLen = len;
    unsigned char rc = 0;

    if (buffLen < DIS_TLV_HDR_SZ)
        return 0;

    // Get the tlv type

    while (buffLen >= DIS_TLV_HDR_SZ)
    {
        unsigned char tlvPyldLen = *(buff_p + DIS_TLV_TYPE_FIELD_LEN);

        if (*buff_p == type)
        {
            *pyldLen_p = tlvPyldLen;
            *pyldBuff_pp = (buff_p + DIS_TLV_HDR_SZ);
            rc = 1;
            break;
        }
        else
        {
            buff_p += (DIS_TLV_HDR_SZ + tlvPyldLen);
            buffLen -= (DIS_TLV_HDR_SZ + tlvPyldLen);
        }
    }

    return rc;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void print_usage()
{
    printf("Usage: ./sniffer.exe <serial-device> <chann-nr (1 to 9)> \n");
    printf("On Cywgin, if the COM port is COMa, the corresponding serial port device is /dev/ttySb where b=a-1 \n");
    printf("Example: ./gw.exe /dev/ttyS20 5 \n");
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
unsigned short __ntohs(unsigned char *buff_p)
{
   short u16Val = *buff_p;
   u16Val = (u16Val << 8) | buff_p[1];  
   return u16Val;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __htons(unsigned char *buff_p, unsigned short val)
{
   buff_p[0] = (val >> 8) & 0xff;
   buff_p[1] = (val) & 0xff;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
unsigned int __ntohl(unsigned char *buff_p)
{
   unsigned int u32Val = *buff_p;
   u32Val <<= 8;
   u32Val |= buff_p[1];
   u32Val <<= 8;
   u32Val |= buff_p[2];
   u32Val <<= 8;
   u32Val |= buff_p[3];
   return u32Val;
}


#ifdef __CYGWIN__
    

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int cfgPort(char *serDevName_p, int baudRate)
{
    struct termios newtio;
    struct termios oldtio;
    struct termios latesttio;
    cntxt_s *serialCntxt_p = &uart_cntxt;
    int rc;

    serialCntxt_p->serialFd = open(serDevName_p, O_RDWR | O_NOCTTY );
    if (serialCntxt_p->serialFd < 0)
    {
        printf("Failed to open serial device <%s> - errno<%d> !!\n",
               serDevName_p, errno);  
        return -1;
    }

#if 0
    rc = tcgetattr(serialCntxt_p->serialFd, &oldtio); /* save current port settings */
    if (rc < 0)
    {
        printf("\n tcgetattr() failed !! - rc<%d>, errno<%d> \n", rc, errno);
        return -1;
    }
#endif
    bzero(&newtio, sizeof(newtio));

    rc = cfsetspeed(&newtio, baudRate);
    if (rc < 0)
    {
        printf("\n cfsetspeed() failed !! - rc<%d>, errno<%d> \n", rc, errno);
        return -1;
    }

    newtio.c_cflag = CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;

    // if ((rc = fcntl(serialCntxt_p->serialFd, F_SETOWN, getpid())) < 0)
    // {
    //     printf("\n fcntl failed !! - rc<%d>, errno<%d> \n", rc, errno);
    //     return -1;
    // }

    /* set input mode (non-canonical, no echo,...) */
    newtio.c_lflag = 0;

    newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
    newtio.c_cc[VMIN]     = 1;   /* blocking read until 10 chars received */

    rc = tcsetattr(serialCntxt_p->serialFd, TCSANOW, &newtio);
    if (rc < 0)
    {
        printf("\n tcsetattr() failed !! - rc<%d> / errno<%d> \n", rc, errno);
        return -1;
    }

    rc = tcflush(serialCntxt_p->serialFd, TCIFLUSH);
    if (rc < 0)
    {
        printf("\n tcflush() failed !! - rc<%d> \n", rc);
        return -1;
    }
    
    tcgetattr(serialCntxt_p->serialFd, &latesttio); 
    if (rc < 0)
    {
        printf("\n tcgetattr() failed !! - rc<%d> \n", rc);
        return -1;
    }

    // printf("\nispeed<%d> / ospeed<%d> \n", latesttio.c_ispeed, latesttio.c_ospeed);
    // printf("\niflag<0x%x>/oflag<0x%x>/cflag<0x%x> \n", latesttio.c_iflag, latesttio.c_oflag, latesttio.c_cflag);

    return 1;
}

#else 
                             
/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int cfgPort(char *serDevName_p, int baudRate)
{
   cntxt_s *serialCntxt_p = &uart_cntxt;
   int rc;

   memset(serialCntxt_p, 0, sizeof(cntxt_s));

   serialCntxt_p->serialFd = open((char *)serDevName_p, O_RDWR | O_NOCTTY | O_NDELAY);
   if (serialCntxt_p->serialFd < 0)
   {
       printf("\n<%s> open(%s) failed !! - errno<%d> \n",
              __FUNCTION__, serDevName_p, errno);
       return -1;
   }
   
   // Zero out port status flags
   if (fcntl(serialCntxt_p->serialFd, F_SETFL, 0) != 0x0)
   {
       return -1;
   }

   bzero(&(serialCntxt_p->dcb), sizeof(serialCntxt_p->dcb));

   // serialCntxt_p->dcb.c_cflag |= serialCntxt_p->baudRate;  // Set baud rate first time
   serialCntxt_p->dcb.c_cflag |= baudRate;  // Set baud rate first time
   serialCntxt_p->dcb.c_cflag |= CLOCAL;  // local - don't change owner of port
   serialCntxt_p->dcb.c_cflag |= CREAD;  // enable receiver

   // Set to 8N1
   serialCntxt_p->dcb.c_cflag &= ~PARENB;  // no parity bit
   serialCntxt_p->dcb.c_cflag &= ~CSTOPB;  // 1 stop bit
   serialCntxt_p->dcb.c_cflag &= ~CSIZE;  // mask character size bits
   serialCntxt_p->dcb.c_cflag |= CS8;  // 8 data bits

   // Set output mode to 0
   serialCntxt_p->dcb.c_oflag = 0;
 
   serialCntxt_p->dcb.c_lflag &= ~ICANON;  // disable canonical mode
   serialCntxt_p->dcb.c_lflag &= ~ECHO;  // disable echoing of input characters
   serialCntxt_p->dcb.c_lflag &= ~ECHOE;
 
   // Set baud rate
   cfsetispeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);
   cfsetospeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);

   serialCntxt_p->dcb.c_cc[VTIME] = 0;  // timeout = 0.1 sec
   serialCntxt_p->dcb.c_cc[VMIN] = 1;
 
   if ((tcsetattr(serialCntxt_p->serialFd, TCSANOW, &(serialCntxt_p->dcb))) != 0)
   {
       printf("\ntcsetattr(%s) failed !! - errno<%d> \n",
              serDevName_p, errno);
       close(serialCntxt_p->serialFd);
       return -1;
   }

   // flush received data
   tcflush(serialCntxt_p->serialFd, TCIFLUSH);
   tcflush(serialCntxt_p->serialFd, TCOFLUSH);

   return 1;
}

#endif


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int readPort(unsigned char *buff_p, unsigned int len)
{
   int rdLen, readLeft = len, totRead = 0;
   // fd_set fds;
   // struct timeval tv;

   // FD_ZERO(&fds);
   // FD_SET(uart_cntxt.serialFd, &fds);

   while (readLeft > 0)
   {
      rdLen = read(uart_cntxt.serialFd, buff_p + totRead, readLeft);
      // printf("\n<%s> rdLen<%d> \n", __FUNCTION__, rdLen);
      if (rdLen > 0)
      {
          totRead += rdLen;
          readLeft -= rdLen;
      }
      else
      {
    	  printf("\n<%s> read() failed  - %d !! \n", __FUNCTION__, rdLen);
          return rdLen;
      }
   }

   return totRead;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int writePort(unsigned char *buff_p, unsigned int cnt)
{
   int rc, bytesLeft = cnt, bytesWritten = 0;
    
   // printf("\n<%s> cnt<%d> \n", __FUNCTION__, cnt);
   
   while (bytesLeft > 0)
   {
      rc = write(uart_cntxt.serialFd, buff_p + bytesWritten, bytesLeft);
      if (rc <= 0)
          return -1;
      else
      {
          bytesLeft -= rc;
          bytesWritten += rc;
      }
   }

   return 1;
}

char *MAC_frameTypeList[8] =
{
   "BCN", "DATA", "ACK", "CMD", 
   "RSVD", "RSVD", "RSVD", "RSVD"
};


char *__macCmdsList[ ] = 
{
  "ASSOC_REQ",
  "ASSOC_RESP",
  "DISASSOC_NOTIF",
  "DATA_REQ",
  "LPWMN_ID_CONFLICT_NOTIF",
  "ORPHAN_NOTIF",
  "BCN_REQ",
  "COORD_REALIGNMENT",
  "GTS_REQ"
};

char *__macAssocRespSts[ ] =
{
  "Success",
  "LPWMN at capacity",
  "LPWMN access denied"
};
                      
/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __decodeBcnFrame(unsigned char *pdu_p, int pduLen)
{
   if (pduLen >= MAC_BCN_PYLD_LEN)
   {
       printf("Assoc-Allowed<%c>  Sender-is-Coord<%c> \n",
              pdu_p[1] & MAC_SF_SPEC_ASSOC_PERMIT_BIT_SHIFT_MSK ? 'y' : 'n',
              pdu_p[1] & MAC_SF_SPEC_IS_COORD_BIT_SHIFT_MSK ? 'y' : 'n');
   }
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __decodeAckFrame(unsigned char *pdu_p, int pduLen)
{
   if (pduLen >= (MAC_ACK_PDU_RSSI_FIELD_LEN + MAC_ACK_PDU_LQI_FIELD_LEN))
   {
       int rssi = *((char *)pdu_p), lqi = pdu_p[1];

       printf("rssi<%d>  lqi<%u> \n", rssi, lqi);
   }
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __decodeCmdFramePyld(unsigned char *pdu_p, int pduLen)
{
   if (pduLen >= MAC_CMD_ID_FIELD_LEN)
   {
       unsigned char cmdId = *pdu_p;
       pdu_p += MAC_CMD_ID_FIELD_LEN;
       pduLen -= MAC_CMD_ID_FIELD_LEN;

       if (cmdId < MAC_CMD_ASSOCIATION_REQ || cmdId > MAC_CMD_GTS_REQUEST)
       {
           printf("\nCmd-Type<Unknown (%u)>\n", cmdId);
       }
       else
       {
           printf("\nCmd-Type<%s>\n", __macCmdsList[cmdId - 1]);
       }

       switch (cmdId)
       {
          case MAC_CMD_ASSOCIATION_REQ:
               {
                  // 1 byte command id, 1 byte capability info
                  if (pduLen > 0)
                  {
                      unsigned char capInfo = *pdu_p;
                      printf("Cap Info: Dev-Type<%s> AC-Pwr<%c> Rx-On-Idle<%c> \n",
                             capInfo & (1 << MAC_ASSOC_REQ_CAP_INO_DEV_TYPE_BIT_NR) ? "FFD" : "RFD",
                             capInfo & (1 << MAC_ASSOC_REQ_CAP_INO_POWER_SOURCE_BIT_NR) ? 'y' : 'n',
                             capInfo & (1 << MAC_ASSOC_REQ_CAP_INO_RX_ON_IDLE_BIT_NR) ? 'y' : 'n');
                       
                  }
               }
               break;

          case MAC_CMD_ASSOCIATION_RESP:
               {
                  // 1 byte command id, 2 byte short address, 1 byte association status
                  if (pduLen >= (MAC_SHORT_ADDR_LEN + MAC_ASSOC_RESP_ASSOC_STS_FIELD_LEN))
                  {
                      unsigned char assocSts = *(pdu_p + MAC_SHORT_ADDR_LEN);
                      printf("Assoc Sts<%s> ", 
                             assocSts <= MAC_ASSOC_STS_LPWMN_ACCESS_DENIED ? \
                             __macAssocRespSts[assocSts] : "Unknown");
                      if (assocSts == MAC_ASSOC_STS_SUCCESS)
                      {
                          unsigned short shortAddr = *(pdu_p + 1);
                          shortAddr = (shortAddr << 8) | *(pdu_p);
                          printf(" Allocated Short Address<0x%04x> \n", shortAddr); 
                      }
                      else
                          printf("\n");
                  }
               }
               break;

          default:
               {

               }
               break;
       }
   }
}

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __decodePdu(unsigned char *pdu_p, int pduLen)
{
   unsigned char frameType, secEna, framePending,
                 ackRequired, panIdCompressed, destAddrMode,
                 srcAddrMode, devTypeFFD, rejoinReq;
   unsigned int frameSeqNr, errFlag = 0;
   unsigned short destLPWMNId, destShortAddr, srcLPWMNId, srcShortAddr;
   unsigned char *destExtAddr_p = NULL;
   unsigned char *srcExtAddr_p = NULL;

   if (pduLen < MAC_PDU_HDR_FC_FIELD_LEN)
       return;

   frameType = (*pdu_p) & MAC_FC_FRAME_TYPE_BITS_SHIFT_MSK;
   secEna = (*pdu_p) & MAC_FC_SEC_ENA_BIT_SHIFT_MSK;
   framePending = (*pdu_p) & MAC_FC_PENDING_BIT_SHIFT_MSK;
   ackRequired = (*pdu_p) & MAC_FC_ACK_REQ_BIT_SHIFT_MSK;
   panIdCompressed = (*pdu_p) & MAC_FC_LPWMN_ID_COMP_BIT_SHIFT_MSK;
   devTypeFFD = *(pdu_p) & MAC_FC_DEV_TYPE_BIT_SHIFT_MSK;
   
   if (frameType > MAC_FRAME_TYPE_CMD)
       return;
   
   rejoinReq = *(pdu_p + 1) & MAC_FC_REJOIN_REQUIRED_BIT_SHIFT_MSK;
  
   printf("\nType<%s> Sec<%c> FP<%c> AR<%c> PIC<%c> RFD<%c> RejoinReq<%c> ", 
          MAC_frameTypeList[frameType],
          secEna ? 'y' : 'n',
          framePending ? 'y' : 'n',
          ackRequired ? 'y' : 'n',
          panIdCompressed ? 'y' : 'n',  
          devTypeFFD ? 'n' : 'y',
          rejoinReq ? 'y' : 'n');
   pdu_p ++;

#ifndef SNIFFER_SUPPORTS_SEC
   if (secEna)
       return;
#endif
 
   destAddrMode = (*pdu_p);
   destAddrMode >>= MAC_FC_DAM_BITS_SHIFT;
   destAddrMode &= MAC_FC_DAM_BITS_MSK;

   srcAddrMode = (*pdu_p);
   srcAddrMode >>= MAC_FC_SAM_BITS_SHIFT;
   srcAddrMode &= MAC_FC_SAM_BITS_MSK;
   
   if (destAddrMode == MAC_ADDRESS_MODE_RSVD
       || srcAddrMode == MAC_ADDRESS_MODE_RSVD)
   {
       return; 
   }

   pdu_p ++;
   pduLen -= MAC_PDU_HDR_FC_FIELD_LEN;
   
   if (pduLen < MAC_PDU_HDR_SEQ_NR_FIELD_LEN)
       return;

#if (MAC_PDU_HDR_SEQ_NR_FIELD_LEN == 4)
   frameSeqNr = pdu_p[3];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[2];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[1];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[0];
#elif (MAC_PDU_HDR_SEQ_NR_FIELD_LEN == 2)
   frameSeqNr = pdu_p[1];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[0];
#elif (MAC_PDU_HDR_SEQ_NR_FIELD_LEN == 1)
   frameSeqNr = pdu_p[0];
#else
#error mac pdu header seq number field len not defined !!
#endif

   printf("Seq-Nr<%u> ", frameSeqNr);

   pdu_p += MAC_PDU_HDR_SEQ_NR_FIELD_LEN;
   pduLen -= MAC_PDU_HDR_SEQ_NR_FIELD_LEN;
   
   switch (destAddrMode)
   {
       case MAC_ADDRESS_MODE_SHORT_ADDR:
       case MAC_ADDRESS_MODE_EXTENDED_ADDR:
            {
               if (pduLen >= MAC_LPWMN_ID_LEN)
               {
#ifndef MAC_LPWMN_ID_ELIDED
                   destLPWMNId = pdu_p[1];
                   destLPWMNId = (destLPWMNId << 8) | pdu_p[0];
                   pdu_p += MAC_LPWMN_ID_LEN;
                   pduLen -= MAC_LPWMN_ID_LEN;
#endif

                   if (destAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
                   {
                       if (pduLen >= MAC_SHORT_ADDR_LEN)
                       { 
                           destShortAddr = pdu_p[1];
                           destShortAddr = (destShortAddr << 8) | pdu_p[0];
                           pdu_p += MAC_SHORT_ADDR_LEN;
                           pduLen -= MAC_SHORT_ADDR_LEN;
                       }
                       else 
                           errFlag = 1;
                   }
                   else
                   {
                       if (pduLen >= MAC_EXT_ADDR_LEN)
                       {
                           destExtAddr_p = pdu_p; 
                           pdu_p += MAC_EXT_ADDR_LEN;
                           pduLen -= MAC_EXT_ADDR_LEN;
                       }
                       else 
                           errFlag = 1;
                   }
               }
               else
                   errFlag = 1;
            }
            break;   
           
       case MAC_ADDRESS_MODE_NO_ADDR:
       default:
            break;   
   }

   if (errFlag)
       return;
       
   if (destAddrMode != MAC_ADDRESS_MODE_NO_ADDR)
   {
       printf("\n");
#ifndef MAC_LPWMN_ID_ELIDED
       printf("Dest LPWMN-Id<0x%04x> / ", destLPWMNId);
#endif
       if (destAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
           printf("Dest Short<%u>", destShortAddr);
       else
       {
           printf("Ext<%x:%x:%x:%x:%x:%x:%x:%x:>", 
                  destExtAddr_p[0], destExtAddr_p[1], destExtAddr_p[2], destExtAddr_p[3], 
                  destExtAddr_p[4], destExtAddr_p[5], destExtAddr_p[6], destExtAddr_p[7]);
       }
   }
   else
       printf("\nDest: None");

   switch (srcAddrMode)
   {
       case MAC_ADDRESS_MODE_SHORT_ADDR:
       case MAC_ADDRESS_MODE_EXTENDED_ADDR:
            {
               if (panIdCompressed)
               {
                   if (destAddrMode == MAC_ADDRESS_MODE_NO_ADDR)
                       errFlag = 1;
                   else
                   {
                       srcLPWMNId = destLPWMNId;
                   }
               }
               else
               {
                   if (pduLen >= MAC_LPWMN_ID_LEN)
                   {
#ifndef MAC_LPWMN_ID_ELIDED
                       srcLPWMNId = pdu_p[1];
                       srcLPWMNId = (srcLPWMNId << 8) | pdu_p[0];
                       pdu_p += MAC_LPWMN_ID_LEN;
                       pduLen -= MAC_LPWMN_ID_LEN;
#endif
                   }
                   else
                       errFlag = 1;
               }

               if (errFlag)
                   break; 

               if (srcAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
               {
                   if (pduLen >= MAC_SHORT_ADDR_LEN)
                   { 
                       srcShortAddr = pdu_p[1];
                       srcShortAddr = (srcShortAddr << 8) | pdu_p[0];
                       pdu_p += MAC_SHORT_ADDR_LEN;
                       pduLen -= MAC_SHORT_ADDR_LEN;
                   }
                   else 
                       errFlag = 1;
               }
               else
               {
                   if (pduLen >= MAC_EXT_ADDR_LEN)
                   {
                       srcExtAddr_p = pdu_p; 
                       pdu_p += MAC_EXT_ADDR_LEN;
                       pduLen -= MAC_EXT_ADDR_LEN;
                   }
                   else 
                       errFlag = 1;
               }
            }
            break;
 
       case MAC_ADDRESS_MODE_NO_ADDR:
       default:
            break;   
   }

   if (errFlag)
       return;
 
   if (srcAddrMode != MAC_ADDRESS_MODE_NO_ADDR)
   {
       printf("\n");
#ifndef MAC_LPWMN_ID_ELIDED
       printf("Src  LPWMN-Id<0x%04x> / ", srcLPWMNId);
#endif
       if (srcAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
           printf("Src  Short<%u>\n", srcShortAddr);
       else
       {
           printf("/ Ext<%x:%x:%x:%x:%x:%x:%x:%x:>\n", 
                  srcExtAddr_p[0], srcExtAddr_p[1], srcExtAddr_p[2], srcExtAddr_p[3], 
                  srcExtAddr_p[4], srcExtAddr_p[5], srcExtAddr_p[6], srcExtAddr_p[7]);
       }
   }
   else
   {
       printf("\nSrc LPWMN-Id<None> / Src Short<None>");
   }

   switch (frameType)
   {
      case MAC_FRAME_TYPE_BEACON:
           __decodeBcnFrame(pdu_p, pduLen);
           break;

      case MAC_FRAME_TYPE_DATA:
           break;

      case MAC_FRAME_TYPE_ACK:
           __decodeAckFrame(pdu_p, pduLen);
           break;

      case MAC_FRAME_TYPE_CMD:
           __decodeCmdFramePyld(pdu_p, pduLen);
           break;
 
      default:
           break;
   }
}
   

unsigned int pktSniffedCnt = 0;               
float lastRcvdPktHighResTimeStamp =  0;

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int __processRcvdMsg(unsigned char *buff_p, int offset, int currMsgType, int pyldLen)
{
   int rc = 0;

   if (verbose)
       printf("<%s> msgType<0x%x> / pyldLen <%d> \n", __FUNCTION__, currMsgType, pyldLen);

   if (verbose)
   {
       int idx;
        
       for (idx = 0; idx<pyldLen; idx++)
       {
            if (idx % 8 == 0)
                printf("\n <%s> : ", __FUNCTION__);
            printf(" 0x%x ", buff_p[offset + idx]);
       }
       printf("\n");
   }

   switch (currMsgType)
   {
      case LPWMN_GW_MSG_TYPE_RELAY_FROM_NODE:
           {
              if (verbose)
                  printf("<%s> rcvd msg type RELAY_FROM_NODE \n", __FUNCTION__);

              buff_p += offset;

              if (pyldLen >= (LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN   
                              + LPWMN_MSG_RSSI_LEN 
                              + LPWMN_MSG_CORR_LEN
                              + SNIFFER_TIME_STAMP_SECS_FIELD_LEN
                              + SNIFFER_TIME_STAMP_32_KHZ_CNTR_FIELD_LEN))
              {
                  unsigned short frameIdx = __ntohs(buff_p);
                  int rssi = *((char *)(buff_p + LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN));
                  int lqi = *((char *)(buff_p + LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN + LPWMN_MSG_RSSI_LEN));
                  char *timeStr_p = ctime(&frameRxTime);
                  int currRcvdPktHighResTSOff = LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN  
                                     + LPWMN_MSG_RSSI_LEN
                                     + LPWMN_MSG_CORR_LEN; 
                  unsigned int highResTimeStampSecs = __ntohl(buff_p + currRcvdPktHighResTSOff);
                  unsigned int highResTimeStamp32KHzCntr = __ntohs(buff_p + currRcvdPktHighResTSOff + SNIFFER_TIME_STAMP_SECS_FIELD_LEN);
                  float delta;

                  // 32 KHz tick count converted to  microsecs

                  // highResTimeStamp32KHzCntr value can range from 0 to 2000000

                  float currRcvdPktHighResTS = highResTimeStamp32KHzCntr;
                  currRcvdPktHighResTS /= 32768;
                  currRcvdPktHighResTS += highResTimeStampSecs;

                  if (pktSniffedCnt > 0)
                  {
                      delta = currRcvdPktHighResTS;
                      delta -= lastRcvdPktHighResTimeStamp;

                      delta *= 1000;   // convert  to millisecs
                  }
                  else 
                      delta = 0;

                  pktSniffedCnt ++;

                  lastRcvdPktHighResTimeStamp = currRcvdPktHighResTS;

                  timeStr_p[strlen(timeStr_p) - 1] = '\0';

                  if (verbose)
                      printf("<%s> frame Index<%04u> \n", __FUNCTION__, frameIdx);

                  printf("#<%04u> <%s> ts<%f (+ %u millisecs)> rssi<%d dBm> lqi<%d> len<%d>", 
                         frameIdx, timeStr_p, currRcvdPktHighResTS, (unsigned int)delta, rssi, lqi, pyldLen);
                  printf("\n--------------------------------------------------------------------------");

                  buff_p += (LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN 
                             + LPWMN_MSG_RSSI_LEN 
                             + LPWMN_MSG_CORR_LEN
                             + SNIFFER_TIME_STAMP_SECS_FIELD_LEN
                             + SNIFFER_TIME_STAMP_32_KHZ_CNTR_FIELD_LEN);
                             

                  pyldLen -= (LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN 
                              + LPWMN_MSG_RSSI_LEN 
                              + LPWMN_MSG_CORR_LEN
                              + SNIFFER_TIME_STAMP_SECS_FIELD_LEN
                              + SNIFFER_TIME_STAMP_32_KHZ_CNTR_FIELD_LEN);


                  if (verbose)
                      printf("<%s> pyldLen left<0x%x> \n", __FUNCTION__, pyldLen);

                  if (pyldLen > 0)
                  {
                      int idx;

                      rc = 1;


                      // Decode the PDU
                      
                      __decodePdu(buff_p, pyldLen);

                      for (idx=0; idx<pyldLen; idx++)
                      {
                           if (idx % 8 == 0)
                               printf("\n[%03u] : ", idx);
                           printf("0x%02x ", buff_p[idx]);
                      }
                  }
                  printf("\n--------------------------------------------------------------------------\n\n\n");
              }
           }
           break;

      case LPWMN_GW_MSG_TYPE_SET_RADIO_CHANNEL:
           {
              if (pyldLen == 0x1)
              {
                  if (buff_p[offset] == 0x1)
                  {
                      channCfgDone = 1;
                      printf("Sniffer listening on channel #<%d> \n\n", channNr);
                  }
                  else
                  {
                      channCfgDone = 0;
                      printf("request denied !!\n");
                  }
              }
              rc = 1;
           }
           break;

      case UART_MSG_TYPE_ACK:
           {
              unsigned char hdrFlags = buff_p[UART_FRAME_HDR_FLAGS_FIELD_OFF];
    
              if (verbose)
                  printf("Hdr Flags <0x%x> \n", hdrFlags);
   
              if (hdrFlags & UART_ACK_STS_OK_BM)
                  hdrAcked = 1;
              else
                  hdrAcked = 0;
  
              rc = 1;
           }
           break;

      case LPWMN_GW_MSG_TYPE_GET_RADIO_FREQ_BAND:
           {
              printf("%s\n", buff_p + offset);
              rc = 1;
           }
           break;

      case LPWMN_GW_MSG_TYPE_GET_RADIO_MOD_FMT:
           {
              printf("%s\n", buff_p + offset);
              rc = 1;
           }
           break;
      
      case LPWMN_GW_MSG_TYPE_GET_RADIO_BAUD_RATE:
           {
              if (pyldLen == LPWMN_GW_MSG_RADIO_BAUD_RATE_FIELD_LEN)
              {
                  int br = __ntohl(buff_p + offset);
                  printf("%d baud \n", br);
              }
              rc = 1;
           }
           break;
 
      case LPWMN_GW_MSG_TYPE_GET_RADIO_PART_NR:
           {
              printf("%s\n", buff_p + offset);
              rc = 1;
           }
           break;
 
      case LPWMN_GW_MSG_TYPE_GET_RADIO_CARRIER_FREQ:
           {
              if (pyldLen == LPWMN_GW_MSG_RADIO_CARRIER_FREQ_FIELD_LEN)
              {
                  unsigned int u32 = __ntohl(buff_p + offset);
                  float rcf = u32;
                  rcf /= 1000000;
                  printf("%f MHz", rcf);
                  rc = 1;
              }
           }
           break;

      case LPWMN_GW_MSG_TYPE_GET_NWK_CHANNEL:
           {
              if (pyldLen == LPWMN_GW_MSG_RADIO_CHANN_FIELD_LEN)
              {
                  unsigned int chann = __ntohs(buff_p + offset);
                  printf("Radio Channel - %d", chann);
                  // printf("Radio Channel - %d (%d Mhz) \n", chann, 2405 + (chann - 11)*5);
                  rc = 1;
              }
           }
           break;

      default:
          {
             printf("Message type<%d> not handled !! \n", currMsgType);
          }
          break;
    }
 
    return rc;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int __buildSendHdr(int msgType, unsigned char *pyldBuff_p, int pyldLen)
{
   unsigned char *buff_p = serTxBuff;
   unsigned short calcCrc16;
   static unsigned char seqNr = 0x0;
   int rc;
  
   if (verbose) 
       printf("<%s> msgType<0x%x> \n", __FUNCTION__, msgType);

   __htons(buff_p, msgType);
   buff_p += UART_FRAME_HDR_MSG_TYPE_FIELD_LEN;

   *buff_p = 0x0;
   buff_p += UART_FRAME_HDR_FLAGS_FIELD_LEN;

   *buff_p = seqNr ++;
   buff_p += UART_FRAME_HDR_SEQ_NR_FIELD_LEN;

   __htons(buff_p, pyldLen);
   buff_p += UART_FRAME_HDR_PYLD_LEN_FIELD_LEN;

   calcCrc16 = crc16(serTxBuff, UART_FRAME_HDR_HDR_CRC_FIELD_OFF);
   __htons(buff_p, calcCrc16);  // no payload
   buff_p += UART_FRAME_HDR_HDR_CRC_FIELD_LEN;

   if (pyldLen > 0)
   {
       calcCrc16 = crc16(pyldBuff_p, pyldLen);
       __htons(buff_p, calcCrc16);  // payload crc 
   }
   else
       __htons(buff_p, 0x0);  // no payload 

   rc = writePort(serTxBuff, UART_FRAME_HDR_LEN);
   if (rc != 1)
   {
       printf("\nwritePort() failed !!\n");
       rc = 20;
   }

   if (verbose) 
       printf("\nwritePort() done !!\n");

   return rc;
}
      

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int __readSerIntf(int expMsgType) 
{
   int rc, off = 0, readLen, pyldLen, totReadLen = 0;
   int currMsgType = 0xffff, done = 0;

   readLen = UART_FRAME_HDR_PYLD_CRC_FIELD_OFF;

   do
   {
      if (verbose) 
          printf("\noff<%d>/readLen<%d>/totReadLen<%d> \n", off, readLen, totReadLen);

      rc = readPort(serRxBuff + off, readLen);
      if (rc != readLen)
      {
          printf("\nreadPort() failed !! ................. \n");
          close(uart_cntxt.serialFd);
          rc = 10;
          break;
      }

      time(&frameRxTime);

      totReadLen += readLen;
      off += readLen;

      switch (totReadLen)
      {
         case UART_FRAME_HDR_PYLD_CRC_FIELD_OFF: 
              {
                  int idx;
                  unsigned short calcCrc16, rxdCrc16;

                  // Get the message length

                  if (verbose)
                  {
                      printf("\n-------------------------------------------------------------------------------------------------");
                      printf("\nRead <%d> bytes --- ", readLen);
                      for (idx=0; idx<totReadLen; idx++)
                           printf(" <0x%x> ", serRxBuff[idx]);
                  }

                  calcCrc16 = crc16(serRxBuff, UART_FRAME_HDR_HDR_CRC_FIELD_OFF);
                  rxdCrc16 = serRxBuff[UART_FRAME_HDR_HDR_CRC_FIELD_OFF];
                  rxdCrc16 = (rxdCrc16 << 8) + serRxBuff[UART_FRAME_HDR_HDR_CRC_FIELD_OFF + 1];
      
                  if (verbose)
                      printf("\ncalc-crc16<0x%x> rcvd-crc16<0x%x>\n", calcCrc16, rxdCrc16);

                  if (calcCrc16 != rxdCrc16)
                  {
                      for (idx=0; idx<UART_FRAME_HDR_PYLD_CRC_FIELD_OFF-1; idx++)
                           serRxBuff[idx] = serRxBuff[idx+1];
                      off = UART_FRAME_HDR_PYLD_CRC_FIELD_OFF - 1;
                      readLen = 1;
                      totReadLen = off;
                      break;
                  }

                  pyldLen = serRxBuff[UART_FRAME_HDR_PYLD_LEN_FIELD_OFF];
                  pyldLen = (pyldLen << 8) |  serRxBuff[UART_FRAME_HDR_PYLD_LEN_FIELD_OFF + 1];

                  currMsgType = serRxBuff[UART_FRAME_HDR_MSG_TYPE_FIELD_OFF];
                  currMsgType = (currMsgType << 8) | serRxBuff[UART_FRAME_HDR_MSG_TYPE_FIELD_OFF + 1];

                  if (verbose)
                      printf("\nMessage Type<%d> / Length<%d>", currMsgType, pyldLen);                   

                  readLen = pyldLen + UART_FRAME_HDR_PYLD_CRC_FIELD_LEN;
              }
              break;

         default:
              {
                  if (currMsgType == expMsgType)
                  {
                      if (__processRcvdMsg(serRxBuff, UART_FRAME_HDR_LEN, currMsgType, pyldLen) == 1)
                          done = 1;
                  }
        
                  readLen = UART_FRAME_HDR_PYLD_CRC_FIELD_OFF;
                  totReadLen = 0;
                  off = 0;
              }
              break;
      }
   } while (done == 0x0);

   if (done == 1)
       rc = 0;
   else
       rc = 6;

   return rc;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int __setRadioChannReqHndlr(const char *arg_p)
{
   int rc;
   unsigned char pyldBuff[LPWMN_GW_MSG_RADIO_CHANN_FIELD_LEN];

   if (verbose)
       printf("\n <%s> entry .... %s \n", __FUNCTION__, arg_p);

   rc = sscanf(arg_p, "%d", &channNr);
   if (rc != 1 
       || (channNr < 1 || channNr > 9))
   {
       printf("Please specify valid channel number (1 - 9) !! \n");
       return 5; 
   }

   __htons(pyldBuff, channNr);

   rc = __buildSendHdr(LPWMN_GW_MSG_TYPE_SET_RADIO_CHANNEL, 
                        pyldBuff, LPWMN_GW_MSG_RADIO_CHANN_FIELD_LEN);
   if (rc != 1)
       return rc;

   rc = __readSerIntf(UART_MSG_TYPE_ACK);
   if (rc != 0)
       return rc;

   if (hdrAcked == 0x0)
   {
       printf("Header not acked !! \n");
       return 6;
   }
      
   if (verbose)
       printf("Header acked \n");

   // Send payload
   rc = writePort(pyldBuff, LPWMN_GW_MSG_RADIO_CHANN_FIELD_LEN);
   if (rc != 1)
   {
       printf("<%s> writePort(%d) failed !! \n", 
              __FUNCTION__, LPWMN_GW_MSG_RADIO_CHANN_FIELD_LEN);
       return 7;
   }

   if (verbose)
       printf("request sent ... \n");
       
   rc = __readSerIntf(LPWMN_GW_MSG_TYPE_SET_RADIO_CHANNEL);
   if (rc == 0)
   {
       if (channCfgDone == 1)
           rc = 0;
       else
           rc = 10;
   }

   return rc;
}


unsigned char __testBuff[ ] = 
{
#if 0
  0x0, 0xea, 0xe5, 0x31,
  0x03, 0x18, 0x0f, 0x0f, 0x00, 0x00,
  0xff, 0xff, 0xff, 0xff, 0x07
#endif
  0x0, 0x1, 0xe5, 0x31,
  0x42, 0x98, 
  0x0d, 0x04, 0x00, 0x00, 
  0x55, 0x74, 
  0x09, 0x00, 
  0x01, 0x00, 
  0xdd, 0x2d
};


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int main(int argc, const char* argv[] )
{
    int rc = 0, channNr;
    int currMsgType = 0xffff;
            
#if OFFLINE_TEST_ENA 
    printf("\n");
    __processRcvdMsg(__testBuff, 0, LPWMN_GW_MSG_TYPE_RELAY_FROM_NODE, sizeof(__testBuff));
    return 0;
#endif 
    if (argc < 3)
    {
        print_usage();
        return 1;
    }
      
    if (strcmp(argv[1], "help") == 0x0
        || argv[1][0] == '?'
        || (argc >= 3 && (strcmp(argv[2], "help") == 0x0
                          || argv[2][0] == '?')))
    {
        print_usage();
        return 0;
    }

    if (cfgPort((char *)argv[1], B38400) < 0)
        return 2;

    if (__setRadioChannReqHndlr(argv[2]) != 0) 
    {
        return 3;
    }

    while (1)
    {
       if (__readSerIntf(LPWMN_GW_MSG_TYPE_RELAY_FROM_NODE) != 0x0)
           break;
    }

    return 7;
}
